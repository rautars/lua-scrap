-- Gradient configuration
local g_config = {
	minval = 1,
	maxval = 3,
	steps = 10,
	base_colors = {{r=0, g=0, b=255}, {r=0, g=255, b=0}, {r=255, g=0, b=0}}
}

local convert_to_rgb = function(minval, maxval, val, colors)
	max_index = #colors - 1
	v = (val-minval) / (maxval-minval) * max_index + 1.0
	i1 = math.floor(v)
	i2 = math.min(math.floor(v)+1, max_index + 1)
	f = v - i1
	c1 = colors[i1]
	c2 = colors[i2]
	return {r=math.floor(c1.r + f*(c2.r - c1.r)), g=math.floor(c1.g + f*(c2.g-c1.g)), b=math.floor(c1.b + f*(c2.b - c1.b))}
end

local drawRainbow = function ()
	pox_x_inc = 50
	pos_x = pox_x_inc
	delta = (g_config.maxval - g_config.minval) / g_config.steps
	range = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	for key,value in pairs(range) do
		val = g_config.minval + value * delta
		color = convert_to_rgb(g_config.minval, g_config.maxval, val, g_config.base_colors)
		drawBox(color, pos_x)
		pos_x = pos_x + pox_x_inc
	end

end

function drawBox(color, pos_x)
	love.graphics.setColor(color.r, color.g, color.b)
	love.graphics.rectangle("fill", pos_x, 50, 45, 120 )
end

function love.draw()
    drawRainbow()
end