-- GLOBAL VARIABLES
idCounter = 0
astTable = {}

-- GENERAL FUNCTIONS
function between_coord(c1, c2, cx)
	--simple check if coordinate cx is between c1 and c2
	local result = false
	if c1 <= cx and c2 >= cx then
		result = true
	end
	return result
end

function collcheck (area1, area2)  
    local checkCount = 0
    if between_coord(area1.t, area1.b, area2.t) then
		result = true
		checkCount = checkCount + 1
	elseif between_coord(area1.t, area1.b, area2.b) then
		result = true
		checkCount = checkCount + 1
	end
	if between_coord(area1.l, area1.r, area2.l) then
		result = true
		checkCount = checkCount + 1
	elseif between_coord(area1.l, area1.r, area2.r) then
		result = true	
		checkCount = checkCount + 1
	end
	
	if checkCount > 1 then
		return true
	else
		return false
	end
end

function getCircleArea(x, y, r)
    local area = {}
    area.t = y - r
    area.b = y + r
    area.l = x - r
    area.r = x + r
    return area
end


function getID()
    if idCounter ~= nil then
        idCounter = idCounter + 1
        return idCounter
    else 
        return false
    end
end

function markHiters(obj1, obj2)
	obj1.lht = os.time()
	obj2.lht = os.time()
	obj1.lho = obj2.id
	obj2.lho = obj1.id
end

function umMarkHitters(obj1, obj2)
    obj1.lho = nil
    obj2.lho = nil
end

function checkMarks(obj1, obj2)
	--initialiazation check rename it
	if obj1.lht == nil or obj2.lht == nil then
		return true
	end
	
	local timeCheck = os.time() + 1 -- + 2 sec 
	if obj1.lho ~= obj2.id and obj2.lho ~= obj1.id then
		return true
	elseif obj1.lht > timeCheck and obj2.lht > timeCheck then
		return true
	else
		return false
	end
end

function astCollCheck(astTable)
    local i = 0 --counter
    while astTable[i] ~= nil do
        local currentArea = getCircleArea(astTable[i].coord.x, astTable[i].coord.y, astTable[i].size)
        local ii = 0 --counter
        while astTable[ii] ~= nil do
			coll = 0
			if i ~= ii then
				local areaToCompare = getCircleArea(astTable[ii].coord.x, astTable[ii].coord.y, astTable[ii].size)
				if collcheck (currentArea, areaToCompare) then
					
					
					if checkMarks(astTable[i], astTable[ii]) == true then
						markHiters(astTable[i], astTable[ii])
						bouncedAst(astTable[i], astTable[ii])
						local tttt = os.time() + 5
						print(astTable[i].lho..'#'..astTable[i].lht..' : '..astTable[ii].lho..'#'..astTable[ii].lht..' ['..tttt..']') 
					end
				end
			end
			ii = ii + 1 
        end
        i = i + 1
    end
end

function giveBoostBySize(obj1, obj2)
	local boost = obj1.size
	if obj1.size < obj2.size then
		boost = obj1.size
	end
	obj1.coord.x = obj1.coord.x + (obj1.speed.x * boost)
	obj1.coord.y = obj1.coord.y + (obj1.speed.y * boost)
	obj2.coord.x = obj2.coord.x + (obj2.speed.x * boost)
	obj2.coord.y = obj2.coord.y + (obj2.speed.y * boost)
end

function giveBoost(obj1, obj2, boost)
	obj1.coord.x = obj1.coord.x + (obj1.speed.x * boost)
	obj1.coord.y = obj1.coord.y + (obj1.speed.y * boost)
	obj2.coord.x = obj2.coord.x + (obj2.speed.x * boost)
	obj2.coord.y = obj2.coord.y + (obj2.speed.y * boost)
end

function bouncedAst(obj1, obj2)
val = math.random(1,999)
	-- simple function which change objects direction
	if obj1.speed.x < 0 and obj2.speed.x < 0 then
		obj1.speed.y = obj1.speed.y * (-1)
		obj2.speed.y = obj2.speed.y * (-1)
		giveBoost(obj1, obj2, 2)
		return nil
	end
	if obj1.speed.x > 0 and obj2.speed.x > 0 then
		obj1.speed.y = obj1.speed.y * (-1)
		obj2.speed.y = obj2.speed.y * (-1)
		giveBoost(obj1, obj2, 2)
		return nil
	end
	if obj1.speed.y < 0 and obj2.speed.y < 0 then
		obj1.speed.x = obj1.speed.x * (-1)
		obj2.speed.x = obj2.speed.x * (-1)
		giveBoost(obj1, obj2, 2)
		return nil
	end
	if obj1.speed.y > 0 and obj2.speed.y > 0 then
		obj1.speed.x = obj1.speed.x * (-1)
		obj2.speed.x = obj2.speed.x * (-1)
		giveBoost(obj1, obj2, 2)
		return nil
	end
	if (obj1.speed.y < 0 and obj2.speed.y > 0) or (obj1.speed.y > 0 and obj2.speed.y < 0) then
		obj1.speed.x = obj1.speed.x * (-1)
		obj2.speed.x = obj2.speed.x * (-1)
		obj1.speed.y = obj1.speed.y * (-1)
		obj2.speed.y = obj2.speed.y * (-1)
		giveBoost(obj1, obj2, 3)
		return nil
	end
	if (obj1.speed.x < 0 and obj2.speed.x > 0) or (obj1.speed.x > 0 and obj2.speed.x < 0) then
		obj1.speed.x = obj1.speed.x * (-1)
		obj2.speed.x = obj2.speed.x * (-1)
		obj1.speed.y = obj1.speed.y * (-1)
		obj2.speed.y = obj2.speed.y * (-1)
		giveBoost(obj1, obj2, 3)
		return nil
	end
	
end



----#######################################################################



-- global variables
screenSize = {x=800, y=600}

-- asteroids
function createAsteroid(color, size, speed, coord)
	local asteroidInfo = {}
	asteroidInfo.id = getID()
	asteroidInfo.color = color
	asteroidInfo.size = size
    asteroidInfo.speed = speed
    asteroidInfo.coord = coord
    asteroidInfo.lht = nil -- last hited timestamp
    asteroidInfo.lho = nil -- last hitet object
    return asteroidInfo
end

function createRandomAsteroid()
    local color = {}
    color.r, color.g, color.b = math.random(0,255), math.random(0,255), math.random(0,255)
    local size = math.random(5,30)
    local speed = {x=math.random(1,3), y=math.random(1,3)}
    local directionx = math.random() 
    local directiony = math.random()
    if directionx > 0.5 then
        speed.x = speed.x * (-1)
    end
    if directiony > 0.5 then
        speed.y = speed.y * (-1)
    end
    local coord = {x=math.random(0,screenSize.x), y=math.random(0,screenSize.y)}
    return createAsteroid(color, size, speed, coord)
end


--callculated stuff
val = 0   -- establish a variable for later use
function love.update(dt)
    if love.keyboard.isDown("up") then
        val = val + dt   -- we will increase the variable by 1 for every second the key is held down
    end
end



for i=0, math.random(8,18), 1 do
astTable[i] = createRandomAsteroid()
end

function drawAst(index, value)
    love.graphics.setColor(value.color.r, value.color.g, value.color.b)
    value.coord.x = value.coord.x + value.speed.x 
    value.coord.y = value.coord.y + value.speed.y
    if value.coord.x > screenSize.x then
        value.coord.x = 0
    elseif value.coord.x < 0 then
        value.coord.x = screenSize.x
    end
    if value.coord.y > screenSize.y then
        value.coord.y = 0
    elseif value.coord.y < 0 then
        value.coord.y = screenSize.y
    end
    love.graphics.circle("fill", value.coord.x, value.coord.y, value.size, 5)
end

--draw stuff:
function love.draw()
    love.graphics.setBackgroundColor(255,255,255)
    love.graphics.setColor(255, 0, 0)
    --love.graphics.print("+", math.random(0, 800), math.random(0, 600))
    local x = love.mouse.getX()
    local y = love.mouse.getY()
    love.graphics.print("x: "..x, 50, 50)
    love.graphics.print("y: "..y, 100, 50)
    love.graphics.print("FPS: "..love.timer.getFPS(), 150, 50)
    love.graphics.print("TESTS: "..val, 200, 50)
    
    table.foreach(astTable, drawAst)
    love.graphics.setColor(0, 255, 0)
    astCollCheck(astTable)
    --love.graphics.circle("fill", x, y, 10, 5)

end
